import java.util.ArrayList;
import java.util.List;

/**
 * Created by vladyslav on 12/15/17.
 */
public class Calculation {

    public double getChancesDealerWin(String playerCard, double cardsInTheDeck, Deck deck) {

        double playerCardScore=0;

        if(playerCard.equals("J")){
            playerCardScore = 11;
        }

        else if (playerCard.equals("Q")){
            playerCardScore = 12;
        }

        else if (playerCard.equals("K")){
            playerCardScore = 13;
        }

        else if (playerCard.equals("A")){
            playerCardScore = 14;
        }
        else {
            playerCardScore = Integer.parseInt(playerCard);
        }

        List<Double> listOfBiggercards = new ArrayList<Double>();
        int [] allcards =  {2,3,4,5,6,7,8,9,10,11,12,13,14};


        for (double x : allcards){
            if(playerCardScore < x){
                listOfBiggercards.add(x);
            }

        }

        double amountOfBiggerCards=0;

        for ( double x: listOfBiggercards ){

            String card="";

            if (x==14){
                card="A";
            }
           else if (x==13){
                card="K";
            }
           else if (x==12){
                card="Q";
            }

            else if (x==11){
               card="J";
            }
           else {
                int a = (int) x;
                card=Integer.toString(a);
            }
            amountOfBiggerCards+=deck.getCardsAmount(card);
        }

        double dealerHigher = ( amountOfBiggerCards )/(cardsInTheDeck)* 100;

        return dealerHigher;
    }



    public double getChancesPlayerWin(String playerCard, double cardsInTheDeck, Deck deck) {

        double playerCardScore=0;

        if(playerCard.equals("J")){
            playerCardScore = 11;
        }

        else if (playerCard.equals("Q")){
            playerCardScore = 12;
        }

        else if (playerCard.equals("K")){
            playerCardScore = 13;
        }

        else if (playerCard.equals("A")){
            playerCardScore = 14;
        }
        else {
            playerCardScore = Integer.parseInt(playerCard);
        }



        List <Double> listOfSmallerCards = new ArrayList<Double>();
         int [] allcards =  {2,3,4,5,6,7,8,9,10,11,12,13,14};


        for (double x : allcards){
            if(playerCardScore > x){
                listOfSmallerCards.add(x);
            }

        }

        double amountOfSmallerCards=0;

        for ( double x: listOfSmallerCards ){
           // String card=Double.toString(x);

            String card="";

            if (x==14){
                card="A";
            }
            else if (x==13){
                card="K";
            }
            else if (x==12){
                card="Q";
            }

            else if (x==11){
                card="J";
            }
            else {
                int a = (int) x;
                card=Integer.toString(a);
            }
            amountOfSmallerCards+=deck.getCardsAmount(card);
        }



        double dealerLower = ( amountOfSmallerCards )/(cardsInTheDeck) * 100;

        return dealerLower;
    }



    public double chanceDealerGetsNonFigure (Deck deck){
        double chance = 0;
        return deck.getAmountOfNumerics()/deck.getCardsInTheDeckAmount() * 100;
    }




}

