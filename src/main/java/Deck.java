import java.util.ArrayList;
import java.util.List;

/**
 * Created by vladyslav on 12/15/17.
 */
public class Deck {

    double cardsAmount;
    double amountOf_Aces;
    double amountOf_Kings;
    double amountOf_Quens;
    double amountOf_Jacks;
    double amountOf_10;
    double amountOf_9;
    double amountOf_8;
    double amountOf_7;
    double amountOf_6;
    double amountOf_5;
    double amountOf_4;
    double amountOf_3;
    double amountOf_2;


    public Deck (double cardsAmount){
        this.cardsAmount=cardsAmount;
        this.amountOf_Aces=cardsAmount/13;
        this.amountOf_Kings=cardsAmount/13;
        this.amountOf_Quens=cardsAmount/13;
        this.amountOf_Jacks=cardsAmount/13;
        this.amountOf_10=cardsAmount/13;
        this.amountOf_9=cardsAmount/13;
        this.amountOf_8=cardsAmount/13;
        this.amountOf_7=cardsAmount/13;
        this.amountOf_6=cardsAmount/13;
        this.amountOf_5=cardsAmount/13;
        this.amountOf_4=cardsAmount/13;
        this.amountOf_3=cardsAmount/13;
        this.amountOf_2=cardsAmount/13;
    }

    public double getCardsInTheDeckAmount(){
        return cardsAmount;
    }

    public double getCardsAmount(String card) {

        //double amount=cardsAmount/13;
        double amount = 0;


        switch (card) {
            case "A":
                amount = amountOf_Aces;
                break;
            case "K":
                amount = amountOf_Kings;
                break;
            case "Q":
                amount = amountOf_Quens;
                break;
            case "J":
                amount = amountOf_Jacks;
                break;
            case "10":
                amount = amountOf_10;
                break;
            case "9":
                amount = amountOf_9;
                break;
            case "8":
                amount = amountOf_8;
                break;
            case "7":
                amount = amountOf_7;
                break;
            case "6":
                amount = amountOf_6;
                break;
            case "5":
                amount = amountOf_5;
                break;
            case "4":
                amount = amountOf_4;
                break;
            case "3":
                amount = amountOf_3;
                break;
            case "2":
                amount = amountOf_2;
                break;
        }
        return amount;
    }

    public void showCard(String card){
        decreaseCards(card);
        this.cardsAmount=cardsAmount-1;

    }

    public void decreaseCards(String card){
        switch (card){
            case  "A" : amountOf_Aces--;
            break;
            case "K": amountOf_Kings--;
                break;
            case "Q" : amountOf_Quens--;
                break;
            case "J" :amountOf_Jacks--;
                break;
            case "10" : amountOf_10--;
                break;
            case "9" : amountOf_9--;
                break;
            case "8" : amountOf_8--;
                break;
            case "7" : amountOf_7--;
                break;
            case "6" : amountOf_6--;
                break;
            case "5":
                 amountOf_5--;
                break;
            case "4":
                 amountOf_4--;
                break;
            case "3":
                amountOf_3--;
                break;
            case "2":
                 amountOf_2--;
                break;

        }
    }

    public double getAmountOfNumerics(){
        double amountOfNumerics = 0;
        List<String> cards = new ArrayList<String>();

        for ( int x = 2; x<11 ;x++){
           String cardName = Integer.toString(x);
           cards.add(cardName);
        }

        for (String a : cards){

            amountOfNumerics +=getCardsAmount(a);

        }


        return amountOfNumerics+amountOf_Aces;

    }


    public double getAmountOfFigures(){
        double amountOfFigures = 0;
        String cardName="";
        List<String> cards = new ArrayList<String>();

        for ( int x = 11; x<14 ;x++){

             if (x == 13) {
                cardName = "K";
            } else if (x == 12) {
                cardName = "Q";
            } else if (x == 11) {
                cardName = "J";
            }

            cards.add(cardName);
        }

        for (String a : cards){

            amountOfFigures +=getCardsAmount(a);

        }

        return amountOfFigures;

    }



}
