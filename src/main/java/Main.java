import java.util.Scanner;

/**
 * Created by vladyslav on 12/15/17.
 */
public class Main {

    public static void main(String[] args) {

        Deck deck=new Deck(520);
        Calculation calculate = new Calculation();
        Scanner reader = new Scanner(System.in);
        String card;
        while (deck.cardsAmount >0){
            System.out.println("Enter Players card ");
           // String playerCard = reader.next();
            card=reader.next();
            deck.showCard(card);
            double cardsInDeck=deck.getCardsInTheDeckAmount();
            double specificCardLeft = deck.getCardsAmount(card);
            double numerics = deck.getAmountOfNumerics();
            double chanceForUserWin=calculate.getChancesPlayerWin(card,deck.getCardsInTheDeckAmount(),deck);
            double chanceForDealerWin = calculate.getChancesDealerWin(card,deck.getCardsInTheDeckAmount(),deck);
            double dealerGetsNumeric=calculate.chanceDealerGetsNonFigure(deck);

            System.out.println(cardsInDeck);

            System.out.println(card+" in the deck left "+ specificCardLeft);

            System.out.println("Amount of Numerics: "+ numerics);


            System.out.println("Amount of Figures: "+ deck.getAmountOfFigures());

            System.out.println("Dealer Win : " + chanceForDealerWin + " %");
            System.out.println("Player Win : " + chanceForUserWin+ " %");


            System.out.println("Dealer gets NON figure card: " + dealerGetsNumeric +  " %");


            System.out.println("Chance to Player win and dealers card is NON numeric : " + chanceForUserWin * dealerGetsNumeric/10000);
            System.out.println("Chance to Dealer win and dealers card is NON numeric : " + chanceForDealerWin * dealerGetsNumeric/10000);


            String winingBet="";
            if (chanceForUserWin > chanceForDealerWin*2 ){
               winingBet="Bet on Player";
            }

            else if ( chanceForDealerWin > chanceForUserWin*2){
                 winingBet="Bet on Dealer";
            }

            else if (dealerGetsNumeric > 78 ){
                 winingBet="Bet on Dealer Will Get NON numeric";
            }

            else {
                 winingBet="Skip this BET";
            }





            System.out.println( winingBet);

            System.out.println("Enter Dealers card ");
           // String dealerCard = reader.next();
            card=reader.next();
            deck.showCard(card);
        }


    }



}
